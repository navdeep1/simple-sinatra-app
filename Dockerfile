FROM ruby:latest
RUN apt-get update -qq && apt-get install -y build-essential
ENV APP_HOME /rea
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
ADD Gemfile* $APP_HOME/
RUN bundle install
ADD . $APP_HOME
EXPOSE 5000
CMD bundle exec ruby helloworld.rb -p 5000 -o 0.0.0.0
