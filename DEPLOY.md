## Documentation on Deployment

Create the docker image using the given Dockerfile.
```
docker build -t navdeep/ruby-app .
```

The image is present in public docker hub by following name.

```
https://hub.docker.com/repository/docker/navdeep/ruby-app
```

Run the Image as below.
```
docker pull navdeep/ruby-app
docker run -d -p 80:4567 navdeep/ruby-app
```
